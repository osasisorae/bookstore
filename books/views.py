from django.shortcuts import render
from django.views.generic import ListView
from .models import Book

class BookListView(ListView):
    """
    This class specifies the model to use for our books view and the html template to display our books
    """
    model = Book
    template_name = 'book_list.html'
